' ### Version 3 doesn't need QTP to copy component script to local
' But he script has to run with C:\Windows\SysWOW64\wscript.exe or cscript.exe
' \Windows\SysWOW64\cscript ReadBPScriptV3.vbs or
' \Windows\SysWOW64\wscript ReadBPScriptV3.vbs
'

Function SyncCopyScript(strQCPath, strLocalPath)
	Set objFSO = CreateObject("Scripting.FileSystemObject")
    'Set qtApp = CreateObject("QuickTest.Application")
    'qtApp.Launch
	'qtApp.Visible = True
	Set tdc = CreateObject("TDApiOle80.TDConnection")
	tdc.InitConnectionEx "http://hpqcenter:8080/qcbin"
	tdc.Login "jisqhx3","wokbot4"
	tdc.Connect "JBHUNT", "DailyTesting"
	REM If Not qtApp.TDConnection.IsConnected Then
		REM MsgBox "Connect QTP to QC then try again..."
	REM End If
	'strQCPath = "Components\MIB\MG"
	Set objFilter = tdc.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.Filter
	objFilter.Filter("CO_NAME") = "Not(""* X"")"
	Set objComponentList = tdc.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.NewList(objFilter.Text) ' ### all components under the folder strQCPath with filter not("* X")
	intCount = objComponentList.Count ' component count under the folder
'	Dim ComponentList()
'	ReDim ComponentList(intCount)
'	strDisplay = ""
'	For i = 1 to intCount
'		strDisplay = strDisplay & i & " -> " & objFilter.Fields.Item(i) & vbCrLf
'	Next
'	Reporter.ReportNote strDisplay
	For i = 1 to intCount
		strName = objComponentList.Item(i).Name ' each component's name
		'ComponentList(i) = strName
        Set objES = objComponentList.Item(i).ExtendedStorage(0)
        strClientPath = objES.ClientPath
        objES.Load "", True ' Load and sync the ExtendedStorage folder before copy the script
        objFSO.CopyFile strClientPath & "\Action1\Script.mts", strLocalPath & strName & ".vbs"
	Next
	SyncCopyScript = intCount
End Function

strQCPath = "Components\MIB\MG"
strLocalPath = "C:\tmp\mg\"
SyncCopyScript strQCPath, strLocalPath
