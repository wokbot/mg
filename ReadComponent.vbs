
Dim temp, key
Const intL = 32, intU = 126

Sub PasswordTestCommented
    temp = "plaintext"
    key = "TMS"

    temp = Encrypt(temp,key)
    WScript.Echo temp
    temp1 = "*cdXZdm"
    temp = Decrypt(temp,key)
    WScript.Echo temp
    WScript.Echo Decrypt(temp1,key)

    'wscript.echo ModShift(170, 32, 127)
End Sub

Function ModShift(intNumber)
    ModShift = ((intNumber - intL) mod (intU - intL)) + intL
    Do While ModShift < intL
        ModShift = ModShift + intU -intL
    Loop
End Function

Function Decrypt(str,key)
 Dim lenKey, KeyPos, LenStr, x, Newstr
 
 Newstr = ""
 lenKey = Len(key)
 KeyPos = 1
 LenStr = Len(Str)
 
 str=StrReverse(str)
 For x = LenStr To 1 Step -1
      '  WScript.Echo ModShift(asc(Mid(str,x,1)) - Asc(Mid(key,KeyPos,1)))
      Newstr = Newstr & chr(ModShift(asc(Mid(str,x,1)) - Asc(Mid(key,KeyPos,1))))
      KeyPos = KeyPos+1
      If KeyPos > lenKey Then KeyPos = 1
      Next
      Newstr=StrReverse(Newstr)
      Decrypt = Newstr
End Function

' ### This doesn't need QTP to copy component script to local
' But the script has to run with C:\Windows\SysWOW64\wscript.exe or cscript.exe
' \Windows\SysWOW64\cscript ReadComponent.vbs <arguments> ... or
' \Windows\SysWOW64\wscript ReadComponent.vbs <arguments> ...
'

Function SyncCopyScript(strCommand, strQCPath, strLocalPath, strFilter)
	Set objFSO = CreateObject("Scripting.FileSystemObject")
    ' ### If using QTP
    'Set qtApp = CreateObject("QuickTest.Application")
    'qtApp.Launch
	'qtApp.Visible = True
	'If Not qtApp.TDConnection.IsConnected Then
		'MsgBox "Connect QTP to QC then try again..."
	'End If
    ' ### when not using QTP
	Set tdc = CreateObject("TDApiOle80.TDConnection")
	tdc.InitConnectionEx "http://hpqcenter:8080/qcbin"
    strPassword = Decrypt("+cdXZdm", "TMS")
	tdc.Login "jisqhx3",strPassword
	tdc.Connect "JBHUNT", "DailyTesting"
	'strQCPath = "Components\MIB\MG"
	Set objFilter = tdc.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.Filter
	objFilter.Filter("CO_NAME") = strFilter
    'objFilter.Filter("CO_NAME") = "Not(""* X"")"
	Set objComponentList = tdc.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.NewList(objFilter.Text) ' ### all components under the folder strQCPath with filter not("* X")
'    WScript.Echo objFilter.Text
	intCount = objComponentList.Count ' component count under the folder
'	Dim ComponentList()
'	ReDim ComponentList(intCount)
'	strDisplay = ""
'	For i = 1 to intCount
'		strDisplay = strDisplay & i & " -> " & objFilter.Fields.Item(i) & vbCrLf
'	Next
'	Reporter.ReportNote strDisplay
    strNameList =""
	For i = 1 to intCount
		strName = objComponentList.Item(i).Name ' each component's name
        strNameList = strNameList & vbCrlf & i & ": """ & strLocalPath & strName & ".vbs"""
        If strCommand = "s" Then
            WScript.StdOut.Write "Save " & i & ": """ & strLocalPath & strName & ".vbs"""
            'ComponentList(i) = strName
            Set objES = objComponentList.Item(i).ExtendedStorage(0)
            strClientPath = objES.ClientPath
            objES.Load "", True ' Load and sync the ExtendedStorage folder before copy the script
            WScript.StdOut.WriteLine " done"
            objFSO.CopyFile strClientPath & "\Action1\Script.mts", strLocalPath & strName & ".vbs"
        End If
	Next
    If strCommand = "u" Then
        WScript.Echo "Upload Not Implemented Yet."
        strCommand = "l"
    End If
    If strCommand = "l" Then
        WScript.Echo "List:" & vbCrLf & strNameList
    End If
	SyncCopyScript = intCount
End Function

Sub TestCommented2
strQCPath = "Components\MIB\MG"
strLocalPath = "C:\tmp\mg\"
strFilter = """trials LOCKED"""
WScript.Echo strQCPath
WScript.Echo strLocalPath
WScript.Echo strFilter & vbCrlf
'SyncCopyScript strQCPath, strLocalPath, strName
End Sub

Set objArgs = WScript.Arguments
'WScript.Echo objArgs.Count
blnHelp = False
If objArgs.Count < 3 Then
    blnHelp = True
ElseIf Not (objArgs.Item(0) = "l" or objArgs.Item(0) = "s" or objArgs.Item(0) = "u") Then
    blnHelp = True
End If
If blnHelp Then
    WScript.Echo vbCrlf & "Read script from a component in QC and save to local drive as ComponentName.vbs" & vbCrlf & vbCrlf & _
    "Usage: " & "ReadComponent <command> <QCPath> <LocalPath> [<componentNameFilter>]"  & vbCrlf & vbCrlf & _
    "<command>" & vbCrlf & _
    "l: List Component Names" & vbCrlf & _
    "s: Save Component scripts from QC to local drive" & vbCrlf & _
    "u: upload Component scripts from local drive to QC" & vbCrlf & vbCrlf & _
    "QCPath: Path in QC Components module. e.g. ""Components\MIB\MG""" & vbCrlf & _
    "LocalPath: Local drive path. e.g. ""C:\tmp\mg\""" & vbCrlf & _
    "Component Filter for Name field only: Name of the component. e.g. ""test comp1"" or ""mg*"", default is *"  & vbCrlf & vbCrlf & _
    "Note: Filter supports wildcard, using ~ to replace double quote." & vbCrlf & _
    "e.g. names that not end with ""space X"": ""Not(~* X~)"" for ""Not(""* X""))"""
    WScript.Quit
End If

'Commented2

Sub MainSub
    strCommand = objArgs.Item(0)
    strQCPath = objArgs.Item(1)
    strLocalPath = objArgs.Item(2)
    'WScript.Echo objArgs.Item(2)
    If objArgs.Count > 3 Then
        strFilter = replace(objArgs.Item(3), "~", chr(34))
    Else
        strFilter = """*"""
    End If
    If Right(strLocalPath, 1) <> "\" Then
        strLocalPath = strLocalPath & "\"
    End If


    WScript.Echo "QC Path: " & strQCPath
    WScript.Echo "Local Path: " & strLocalPath
    WScript.Echo "Name Filter: " & strFilter & vbCrlf

    SyncCopyScript strCommand, strQCPath, strLocalPath, strFilter
End Sub

MainSub