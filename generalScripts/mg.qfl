' Function List
' - Function SaveDatatableToResource - To save run time datatable to QC Resources module
' - Sub ReportStrComp - Compare 2 strings and report result
' - Sub ReportInStr - Check if the 2nd string is part of 1st string then report the result 





'-------------------------------------------------------------------------------
'Function Name: SaveDatatableToResource
'Description: The function allows run time datatable to be saved to QC Test
'             Resources module.
'             The resource name is specified in the function parameter and
'             the function will look for the resource name in QC Test Resources
'             module, then upload datatable to replace it.
'Created By: Hao Xin 
'Example: SaveDatatableToResource "test.xls"
'-------------------------------------------------------------------------------

Function SaveDatatableToResource(strResourceName)
    Dim qcConn, strTempFolder, intCount, strName
    ' Connect To QC
    Set qcConn = QCUtil.QCConnection

    ' Set Temp Folder
    strTempFolder = environment("SystemTempDir") 

    Set ResourceFactory = qcConn.QCResourceFactory
    Set ResourceList = ResourceFactory.NewList("")
    Set Resource = Nothing

	' Traverse all items to find the resource 
    For intCount = 1 To ResourceList.Count
		strName = ResourceList.Item(intCount).Name
		If UCase(strName) = UCase(strResourceName) Then
			Set Resource = ResourceList.Item(intCount)
		End If
    Next
    Set ResourceFactory = Nothing
    Set ResourceList = Nothing

    ' Assign the resource name (in QC11 it was blank)
	Resource.FileName = strResourceName
    
    ' Export Datatable to Temp Directory
    Datatable.Export strTempFolder & "\" & Resource.Filename

    Resource.Post
    ' Upload the exported datatable from temp folder to QC
    Resource.UploadResource strTempFolder, True
End Function

'-------------------------------------------------------------------------------
'Function Name: ReportStrComp
'Description: Compare 2 strings and send to test result
'Created By: Hao Xin 
'-------------------------------------------------------------------------------
Sub ReportStrComp(strExp, strAct, strDes)
	If StrComp(strAct, strExp, 1) = 0 Then
		Reporter.Reportevent micPass, strDes,"checkpoint passed"
	Else
		Reporter.ReportEvent micFail, strDes, "checkpoint failed" & vbNewLine & "Expected:[" & strExp & "] Actual: [" & strAct & "]"
	End If
End Sub

'-------------------------------------------------------------------------------
'Function Name: ReportInStr
'Description: Check if second string is part of first string then report the result
'Created By: Hao Xin 
'-------------------------------------------------------------------------------
Sub ReportInStr(strBig, strSmall, strDes)
	If InStr(1, strBig, strSmall, 1) <> 0 Then
		Reporter.Reportevent micPass, strDes,"checkpoint passed"
	Else
		Reporter.ReportEvent micFail, strDes, "checkpoint failed" & vbNewLine & "Small:[" & strSmall & "] Big: [" & strBig & "]"
	End If
End Sub


'****************************************************************************** 
'Function: ExistOneOrMany
'    Waits until one or many objects found matching with given object 
'    description.Usually it changes the default behaviour of exist 
'    returning false when multiple objects Exist. 
'
'Comment:
'    It is possible to register this function to any object class using:
'    RegisterUserFunc <Class>, "Exist", "ExistOneOrMany"
'
'Arguments:
'    ByRef obj - Any Object
'    ByVal intTimeoutMSec
'
'Returns:
'    True     -  One or more objects Exist matching the description
'    False    -  No objects exist matching the description 
'
'Developed By:
'    Anand Tambey
'
'Date:
'    15-June-2011
'
'****************************************************************************** 
Public Function ExistOneOrMany(ByRef obj, ByVal intTimeoutMSec)
'****************************************************************************** 

    Dim Parent, oDesc, Props, PropsCount, MaxIndex, Objs 

    If IsEmpty(obj.GetTOProperty("parent")) Then 
          Set Parent = Desktop 
    Else 
          Set Parent = obj.GetTOProperty("parent") 
    End If 

    Set oDesc = Description.Create 
    Set Props = obj.GetTOProperties 
    
   PropsCount = Props.Count - 1 

    For i = 0 to PropsCount 
         oDesc(Props(i).Name).Value = Props(i).Value 
    Next 

    Set Objs = Parent.ChildObjects(oDesc) 

    MaxIndex= Objs.Count 

    'This logic can be changed/extended as per need

    If MaxIndex >=1 Then
   ExistOneOrMany = True
   If  MaxIndex = 1 Then
      Print "1 Exact Match Found"
   Else
      Print "Multiple Matches Found"
   End If
    Else
   ExistOneOrMany = False
    End If
'****************************************************************************** 
End Function
'****************************************************************************** 

RegisterUserFunc "WebElement", "ExistOneOrMany", "ExistOneOrMany",True

