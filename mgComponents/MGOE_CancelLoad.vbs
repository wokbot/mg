﻿Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("name:=TMS").Page("title:=TMS")

objPageMG.Sync
'objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Click
objPageMG.Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='More Actions...']").Click
objPageMG.Frame("name:=Detail").Link("innertext:=Cancel Transport").Click
objPageMG.Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Cancel Transport']").Click

' ### Verify the load is not in the list anymore. implement later
' ### problem: after cancelling, the load is not displayed, but still can be found by script. this causes multiple match later unless close/reopen the app
