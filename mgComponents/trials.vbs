﻿Set WScript = CreateObject("WScript.Shell")
'------------------------------------------------------------'
'@Function Name: QCSaveToResource
'@Documentation Saves a text file to the QC Test Resources module
'@Created By: Joe Colantonio
'@Return Values: NONE
'@Example: QcSaveToResource "Joe","qcresourcetest.txt","C:\QTPAUTOMATION\DATA","",""
'-----------------------------------------------------------
Function QCSaveToResource(QcResourceName,fileNameToSave,fileToSavePath,resourceType,opt)
'**************************
'Upload a resource
'**************************
Set qcConn = QCUtil.QCConnection
Set oResource = qcConn.QCResourceFactory
Set oCurrentResources =oResource.NewList("")
Set oNewResource = Nothing
resourceCount = oCurrentResources.Count
For iNowResourceNum = 1 To resourceCount
	nowResource =  oCurrentResources.Item(iNowResourceNum).Name
   if UCase(nowResource) = UCase(QcResourceName) then
    Set oNewResource = oCurrentResources.Item(iNowResourceNum)
    resourceFound = "True"
   end if

Next

If resourceFound = "True" Then
 oNewResource.Filename = fileNameToSave
 oNewResource.ResourceType = "Test Resource" 
 oNewResource.Post
 oNewResource.UploadResource fileToSavePath, True
Else
 reporter.ReportEvent micFail,"Did not find a resource in the Test Resource module named " & QcResourceName,"Verify that a resource exist in the QC Test Resource module!"
End If

Set oCurrentResources = Nothing
Set oResource = Nothing

End Function

Function SaveToQC(ResourceName)
    '' Connect To QC
    Set qcc = QCUtil.QCConnection

    '' Set Temp Folder
    TempFolder = environment("SystemTempDir") 

    Set ResourceFactory = qcc.QCResourceFactory
    Set ResourceList = ResourceFactory.NewList("")

'    Set Resource = Nothing
    iTotalItems = ResourceList.Count
    For ItemCtr = 1 To iTotalItems
		CurItem = ResourceList.Item(ItemCtr).Name
		If UCase(CurItem) = UCase(ResourceName) Then
			Set Resource = ResourceList.Item(ItemCtr)
		End If
    Next
    Set ResourceFactory = Nothing
    Set ResourceList = Nothing

    '' Export Datatable to Temp Directory
	' added myself
	Resource.FileName = ResourceName
    Datatable.Export TempFolder & "\" & Resource.Filename

    Resource.Post

    Resource.UploadResource TempFolder, True
End Function

Sub Commented

Dim fso
Set fso = CreateObject("Scripting.FileSystemObject")

DataTable.AddSheet "LoadNumber"
XLdatafile = PathFinder.Locate("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\test.xls")

If Len(XLdatafile) = 0 Then
	MsgBox("The Data Table is not found in QC")
Else
	DataTable.ImportSheet XLdatafile, "LoadNumber", "LoadNumber"
End If
'
intRow = datatable.GetSheet("LoadNumber").GetRowCount
datatable.GetSheet("LoadNumber").SetCurrentRow(intRow + 1)

Set TypeLib = CreateObject("Scriptlet.TypeLib")
strUniqueID = Mid(TypeLib.Guid, 2, 8)
DataTable.GetSheet("LoadNumber").GetParameter("LoadNumber").Value=strUniqueID

DataTable.Export("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\test.xls" & "")
'DataTable.ExportSheet XLdatafile, "LoadNumber"
'QCSaveToResource(XLdatafile, "mg.xls", 
'
'SaveToQC "test.xls"
'uploadTestResultToQC
SaveDatatableToResource "test.xls"

End Sub

Function uploadTestResultToQC
 Set objPath  = CreateObject("Scripting.FileSystemObject")
 If Not objPath.FileExists("C:\test\test.xls") Then
    msgbox "not exist  file"
    ExitTest
 End If
 Set objPath = nothing

Set qcConn = QCUtil.QCConnection
Set ResourceFactory = qcConn.QCResourceFactory

' Set ResourceFactory = QCUtil.QCConnection.QCResourceFactory      '事先得连接上QC           
 Set ResourceFilter =  ResourceFactory.Filter
    ResourceFilter.Filter("RSC_NAME")="test.xls"
    Set ResourceList =  ResourceFilter.NewList
    If   ResourceList.Count >0 Then
 For j=1 To ResourceList.Count                   
   Set ResourceFile = ResourceList.Item(j)    
   If  ResourceFile.name = "test.xls"Then
   ResourceFile.FileName = "test.xls"  'API文档上有说明，事先得给一个filename
   ResourceFile.post   '必须先post一下，要不然上传不了
   ResourceFile.UploadResource "C:\test\"&" ",true  '路径后面一定要加上""，要不然也上传不成功，这是最关键的两点
   End If
 next 
 End If  
 Set ResourceFile = nothing
 Set ResourceFilter = nothing
 Set ResourceFactory = nothing
end function


Function SaveDatatableToResource(strResourceName)
    '' Connect To QC
    Set qcConn = QCUtil.QCConnection

    '' Set Temp Folder
    strTempFolder = environment("SystemTempDir") 

    Set ResourceFactory = qcConn.QCResourceFactory
    Set ResourceList = ResourceFactory.NewList("")

    Set Resource = Nothing
    intTotalItems = ResourceList.Count
    For intItemCtr = 1 To intTotalItems
		strCurItem = ResourceList.Item(intItemCtr).Name
		If UCase(strCurItem) = UCase(strResourceName) Then
			Set Resource = ResourceList.Item(intItemCtr)
		End If
    Next
    Set ResourceFactory = Nothing
    Set ResourceList = Nothing

    '' Export Datatable to Temp Directory
	' added myself
	Resource.FileName = strResourceName
    Datatable.Export strTempFolder & "\" & Resource.Filename

    Resource.Post

    Resource.UploadResource strTempFolder, True
End Function


' ### try load functionLibrary from QC
'strFunctionLibraryMG = PathFinder.Locate("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mgfl")
'LoadFunctionLibrary strFunctionLibraryMG
'CollapseAllLoads

' ### parameter: strQCFile contains path and file name, e.g.: "Components\MIB\MG\Login as admin"
Function ReadScript(strQCPath)
   Set qtApp = CreateObject("QuickTest.Application")
    qtApp.Launch
	qtApp.Visible = True
	If Not qtApp.TDConnection.IsConnected Then
		MsgBox "Connect to QC please"
	End If
	qtApp.OpenBusinessComponent strQCPath, False, False
	Set strMyComp = qtApp.BusinessComponent
	Set strMyAct = strMyComp.Actions.Item(1)
	strScript = strMyAct.GetScript
	ReadScript = strScript
End Function

Function WriteScript(strMyScript, strLocalPath)
	Const ForReading = 1, ForWriting = 2, ForAppending = 8
	Const NewFileCreated = True, NewFileNotCreated = False
	Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	'strMyScriptLines = Split(strMyScript, vbCrLf) ' ### not necessary, can just write the whole string
	Set fso=createobject("Scripting.FileSystemObject")
	'Set qfile=fso.CreateTextFile(strLocalPath,True, True) 'Overwrite- True:can be overwritten, False:can't be overwritten; Unicode- True:Unicode, False: ASCII 
    ' not necessary to create the file first, can use OpenTextFile with create switch set to True
	' Test Unicode 试一下含中文能不能完成写操作
	'Output --> New File is created
	'Open the file in writing mode.
	Set qfile=fso.OpenTextFile(strLocalPath, ForWriting, NewFileCreated, TristateTrue) ' iomode, create, format: -1:Unicode, 0:ASCII, -2: system default
	REM For Each strLine in strMyScriptLines ' ### not necessary, can just write the whole string
		REM qfile.Writeline strLine
		REM 'reporter.ReportNote strLine
	REM Next
	'write contents to the file into a single line
    qfile.Write strMyScript
	qfile.Close
	'Release the allocated objects
	Set qfile=nothing
	Set fso=nothing
End Function

Function GetComponentList(strQCPath)
    Set qtApp = CreateObject("QuickTest.Application")
    qtApp.Launch
	qtApp.Visible = True
	If Not qtApp.TDConnection.IsConnected Then
		MsgBox "Connect QTP to QC then try again..."
	End If
	'strQCPath = "Components\MIB\MG"
	Set objFilter = qtApp.TDConnection.TDOTA.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.Filter
	objFilter.Filter("CO_NAME") = "Not(""* X"")"
	Set objComponentList = qtApp.TDConnection.TDOTA.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.NewList(objFilter.Text) ' ### all components under the folder strQCPath with filter not("* X")
	intCount = objComponentList.Count ' component count under the folder
	Dim ComponentList()
	ReDim ComponentList(intCount)
'	strDisplay = ""
'	For i = 1 to intCount
'		strDisplay = strDisplay & i & " -> " & objFilter.Fields.Item(i) & vbCrLf
'	Next
'	Reporter.ReportNote strDisplay
	For i = 1 to intCount
		strName = objComponentList.Item(i).Name ' each component's name
		ComponentList(i) = strName
	Next
    Wait 1
	' ### hard code the list for now
	REM ComponentList(0) = "Login as admin"
	REM ComponentList(1) = "Create a load"
	REM ComponentList(2) = "Find the load"
	REM ComponentList(3) = "Cover - Assign Customer Rate"
	REM ComponentList(4) = "Cover - Accept Quote"
	REM ComponentList(5) = "Cover - Set Appointments"
	REM ComponentList(6) = "Cover - Assign Carrier Rate"
	REM ComponentList(7) = "Cover - Send Carrier Confirmation"
	REM ComponentList(8) = "Cover - Cover"
	REM ComponentList(9) = "Cover - Dispatch"
	REM ComponentList(10) = "Cover - Set ArrivalDeparture"
	REM ComponentList(11) = "CheckWebElement"
	REM ComponentList(12) = "Verify menubar"
	REM ComponentList(13) = "trials"
	GetComponentList = ComponentList
End Function

Sub Commented

Dim arrList
arrList = GetComponentList("Components\MIB\MG")
For i = 1 to UBound(arrList)
    strMyScript = ReadScript("[QualityCenter] Components\MIB\MG\" & arrList(i))
    WriteScript strMyScript, "H:\mg\" & arrList(i) & ".vbs"
Next

End Sub

Function SyncCopyScript(strCommand, strQCPath, strLocalPath, strFilter)
	Set objFSO = CreateObject("Scripting.FileSystemObject")
    ' ### If using QTP
    'Set qtApp = CreateObject("QuickTest.Application")
    'qtApp.Launch
	'qtApp.Visible = True
	'If Not qtApp.TDConnection.IsConnected Then
		'MsgBox "Connect QTP to QC then try again..."
	'End If
    ' ### when not using QTP
	Set tdc = CreateObject("TDApiOle80.TDConnection")
	tdc.InitConnectionEx "http://hpqcenter:8080/qcbin"
    strPassword = Decrypt("*cdXZdm", "TMS")
	tdc.Login "jisqhx3",strPassword
	tdc.Connect "JBHUNT", "DailyTesting"
	'strQCPath = "Components\MIB\MG"
	Set objFilter = tdc.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.Filter
	objFilter.Filter("CO_NAME") = strFilter
    'objFilter.Filter("CO_NAME") = "Not(""* X"")"
	Set objComponentList = tdc.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.NewList(objFilter.Text) ' ### all components under the folder strQCPath with filter not("* X")
'    WScript.Echo objFilter.Text
	intCount = objComponentList.Count ' component count under the folder
'	Dim ComponentList()
'	ReDim ComponentList(intCount)
'	strDisplay = ""
'	For i = 1 to intCount
'		strDisplay = strDisplay & i & " -> " & objFilter.Fields.Item(i) & vbCrLf
'	Next
'	Reporter.ReportNote strDisplay
    strNameList =""
	For i = 1 to intCount
		strName = objComponentList.Item(i).Name ' each component's name
        strNameList = strNameList & vbCrlf & i & ": """ & strLocalPath & strName & ".vbs"""
        If strCommand = "s" Then
            WScript.StdOut.Write "Save " & i & ": """ & strLocalPath & strName & ".vbs"""
            'ComponentList(i) = strName
            Set objES = objComponentList.Item(i).ExtendedStorage(0)
            strClientPath = objES.ClientPath
            objES.Load "", True ' Load and sync the ExtendedStorage folder before copy the script
            objFSO.CopyFile strClientPath & "\Action1\Script.mts", strLocalPath & strName & ".vbs"
            WScript.StdOut.WriteLine " done"
            End If
	Next
    If strCommand = "u" Then
        WScript.Echo "Upload Not Implemented Yet."
        strCommand = "l"
    End If
    If strCommand = "l" Then
        WScript.Echo "List:" & vbCrLf & strNameList
    End If
	SyncCopyScript = intCount
End Function


'strQCPath = "Components\MIB\MG"
'strLocalPath = "C:\tmp\mg\"
'SyncCopyScript strQCPath, strLocalPath
Dim temp, key
Const intL = 32, intU = 126

Sub PasswordTestCommented
    temp = "plaintext"
    key = "TMS"

    temp = Encrypt(temp,key)
    WScript.Echo temp
    temp1 = "*cdXZdm"
    temp = Decrypt(temp,key)
    WScript.Echo temp
    WScript.Echo Decrypt(temp1,key)

    'wscript.echo ModShift(170, 32, 127)
End Sub

Function ModShift(intNumber)
    ModShift = ((intNumber - intL) mod (intU - intL)) + intL
    Do While ModShift < intL
        ModShift = ModShift + intU -intL
    Loop
End Function

Function Decrypt(str,key)
 Dim lenKey, KeyPos, LenStr, x, Newstr
 
 Newstr = ""
 lenKey = Len(key)
 KeyPos = 1
 LenStr = Len(Str)
 
 str=StrReverse(str)
 For x = LenStr To 1 Step -1
      '  WScript.Echo ModShift(asc(Mid(str,x,1)) - Asc(Mid(key,KeyPos,1)))
      Newstr = Newstr & chr(ModShift(asc(Mid(str,x,1)) - Asc(Mid(key,KeyPos,1))))
      KeyPos = KeyPos+1
      If KeyPos > lenKey Then KeyPos = 1
      Next
      Newstr=StrReverse(Newstr)
      Decrypt = Newstr
End Function

Function SyncCopyResource(strCommand, strLocalPath, strFilter)
	Set objFSO = CreateObject("Scripting.FileSystemObject")
    ' ### If using QTP
    'Set qtApp = CreateObject("QuickTest.Application")
    'qtApp.Launch
	'qtApp.Visible = True
	'If Not qtApp.TDConnection.IsConnected Then
		'MsgBox "Connect QTP to QC then try again..."
	'End If
    ' ### when not using QTP
	Set tdc = CreateObject("TDApiOle80.TDConnection")
	tdc.InitConnectionEx "http://hpqcenter:8080/qcbin"
    strPassword = Decrypt("*cdXZdm", "TMS")
	tdc.Login "jisqhx3",strPassword
	tdc.Connect "JBHUNT", "DailyTesting"
	'strQCPath = "Components\MIB\MG"
    ' ### get resource folder filter fields:
    REM for i = 1 to tdc.QCResourceFolderFactory.Filter.Fields.Count
        REM wscript.echo tdc.QCResourceFolderFactory.Filter.Fields.Item(i)
    REM next
    REM Wscript.quit
    
    REM RFO_DESCRIPTION
    REM RFO_ID
    REM RFO_NAME
    REM RFO_PARENT_ID
    REM RFO_PATH
    REM RFO_VER_STAMP
    REM RFO_USER_01    

    ' ### get resource filter fields:
    REM for i = 1 to tdc.QCResourceFactory.Filter.Fields.Count
        REM wscript.echo tdc.QCResourceFactory.Filter.Fields.Item(i)
    REM next
    REM Wscript.quit
    
    REM RSC_HAS_DEPENDENCIES
    REM RSC_DEV_COMMENTS
    REM RSC_CREATED_BY
    REM RSC_CREATION_DATE
    REM RSC_DESCRIPTION
    REM RSC_FILE_NAME
    REM RSC_PARENT_ID
    REM RSC_LOCATION_TYPE
    REM RSC_VTS
    REM RSC_NAME
    REM RSC_FOLDER_NAME
    REM RSC_ID
    REM RSC_TYPE
    REM RSC_VC_CHECKIN_COMMENTS
    REM RSC_VC_CHECKIN_DATE
    REM RSC_VC_CHECKIN_TIME
    REM RSC_VC_CHECKOUT_COMMENTS
    REM RSC_VC_CHECKOUT_DATE
    REM RSC_VC_CHECKOUT_TIME
    REM RSC_VC_CHECKIN_USER_NAME
    REM RSC_VC_CHECKOUT_USER_NAME
    REM RSC_VC_VERSION_NUMBER
    REM RSC_VER_STAMP
    REM RSC_VC_STATUS
    REM RSC_USER_01
    
    
    ' ### get compoent folder filter fields:
    REM for i = 1 to tdc.ComponentFolderFactory.FolderByPath("Components\MIB\MG").ComponentFactory.Filter.Fields.Count
        REM wscript.echo tdc.ComponentFolderFactory.FolderByPath("Components\MIB\MG").ComponentFactory.Filter.Fields.Item(i)
    REM next
    REM Wscript.quit
    
    'wscript.echo strFilter
	Set objFilter = tdc.QCResourceFactory.Filter
	'objFilter.Filter("RSC_NAME") = strFilter
	objFilter.Filter("RSC_FILE_NAME") = strFilter
    'wscript.echo strFilter
    'wscript.echo objFilter.Text
    'objFilter.Filter("CO_NAME") = "Not(""* X"")"
	Set objResourceList = tdc.QCResourceFactory.NewList(objFilter.Text)
'    WScript.Echo objFilter.Text
	intCount = objResourceList.Count
    'wscript.echo intCount
'	Dim ComponentList()
'	ReDim ComponentList(intCount)
'	strDisplay = ""
'	For i = 1 to intCount
'		strDisplay = strDisplay & i & " -> " & objFilter.Fields.Item(i) & vbCrLf
'	Next
'	Reporter.ReportNote strDisplay
    strNameList =""
	For i = 1 to intCount
		strName = objResourceList.Item(i).FileName ' each or Name?
        print strName
		Set objItem = objResourceList.Item(i)

        strNameList = strNameList & vbCrlf & i & ": """ & strLocalPath & strName & """"
        'wscript.echo strNameList
        If strCommand = "s" Then
            print "Save " & i & ": """ & strLocalPath & strName & """"
			objItem.DownloadResource strLocalPath, True
            'ComponentList(i) = strName
            'Set objES = objResourceList.Item(i).ExtendedStorage(0)
            'strClientPath = objES.ClientPath
            'wscript.echo strClientPath
            'objES.Load "", True ' Load and sync the ExtendedStorage folder before copy the script
            'print replace(strClientPath,"APPLICATIONAREA_SCRIPT","") & strName
            'print strLocalPath & strName
            'objFSO.CopyFile replace(strClientPath,"APPLICATIONAREA_SCRIPT","") & strName, strLocalPath & strName
            print " done"
		ElseIf strCommand = "u" Then
			print "Upload " & i & ": """ & strLocalPath & strName & """"
			objItem.Post
			objItem.UploadResource strLocalPath, True
		End If
	Next
    'wscript.echo strCommand
    If strCommand = "u" Then
        'WScript.Echo "Upload Not Implemented Yet."
        'strCommand = "l"
    End If
    If strCommand = "l" Then
        print "List:" & vbCrLf & strNameList
    End If
	SyncCopyResource = intCount
End Function

Sub TestCommented2
strCommand = "u"
strQCPath = "Resources\Test-MIB"
strLocalPath = "C:\tmp\mg\"
strFilter = "hxtest*"
print strQCPath
print strLocalPath
print strFilter & vbCrlf
SyncCopyResource strCommand, strLocalPath, strFilter
End Sub

TestCommented2
