﻿'
'Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("title:=TMS").Page("title:=TMS")
strCoverStep = "Cover"

'Sub Commented
' ### Look for Assign Carrier Rate with class='mg-bubble-title and htmltext='Assign Carrier Rate'
' ### Then look for its child and grandchild <a>/<img>
' ### msgbox objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[contains(concat(' ', normalize-space(@class), ' '), 'mg-bubble-title') and contains(text(), 'Assign Carrier Rate')]/a/img").GetROProperty("html id")
' ### xpath:=//div[@class='mg-bubble-title' and contains(text(), 'Cover')] ' can't use .= because the strings always have trailing spaces
objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Click
'/****
wait 1
'End Sub

objPageMG.Frame("name:=Detail").WebElement("html tag:=SPAN", "innerhtml:=OK").Click

Wait 1 ' ###  try similar to waitforloading later *************************************

CheckEZ strCoverStep, 60

' ### check if the EZ button disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i, objPageMG
	Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote strCoverStep & " - Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub
