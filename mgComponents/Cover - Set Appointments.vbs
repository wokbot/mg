﻿Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
strCoverStep = "Set Appointments"


' ### Check if the EZ Click icon disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i, objPageMG
	Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote strCoverStep & " - Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub

objPageMG.Sync
objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Click

' wait 5
' click first come first served checkbox for drop
Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=(//label[.='First Come, First Served'])[2]")
objElement.WaitProperty "visible", True, 20
objElement.Click

'Click Save button
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Save']").Click

'
'Wait 5 '' for load list population
''
'check if the EZ button disappears
CheckEZ strCoverStep, 60

