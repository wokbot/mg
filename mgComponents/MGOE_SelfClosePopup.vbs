﻿'strMessage =  "Test Run Completed"
'intDelay =  1
strMessage = Parameter("strMessage")
intDelay = Parameter("intDelay")

Set objWS = CreateObject("WScript.Shell")
Set qtApp = CreateObject("QuickTest.Application") 
Set objDefColl = qtApp.BusinessComponent.ParameterDefinitions
intCount = objDefColl.Count
'Print intCount
i = 1
Do While i <= intCount
	Set objItem = objDefColl.Item(i)
	If objItem.Name = "intDelay" and Not IsNumeric(intDelay)Then
		intDelay = objItem.DefaultValue
	End If
	If objItem.Name = "strMessage" and Len("strMessage") = 0 Then
		strMessage = objItem.DefaultValue
	End If
	i = i + 1
Loop
Print strMessage
intButton = objWS.Popup(strMessage, intDelay, strMessage, 64)
Parameter("intButton") = intButton
Print "*** " & Date & " " & Time & "***"
