# README #

Mercury Gate automation project

### What is this repository for? ###

* QTP Business Process Test framework
* Version QTP 11.00
* Folders:
    * **generalScript**: store some code snippets that might be useful elsewhere
    * **mgArchives**: for stuff not using anymore but might be useful later or again
    * **mgComponents**: backup [QualityCenter] Components\MIB\MG\ scripts, or other QC folders specified in ReadComponents.vbs arguments. Run ReadComponent.vbs for more information.
    * **mgDocs**: supporting documents created by me for the project
    * **mgOthersDocs**: supporting documentation from other teams or team members for the project
    * **mgResources**: QC Resources files, sync up periodically manually
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Run ReadBPScript.vbs periodically to backup changes, see steps below.
* Think about how to do the following:
    1. restore them back to QC - implement "u" command for ReadComponent.vbs
    1. explore ways to branch the work

### Backup steps:
1. Manully download files listed in mgResources folder from QC Resources module
1. In Windows command prompt:

``` bash
H:\mg>C:\Windows\SysWOW64\cscript ReadComponent.vbs s "Components\MG\anyFolder" "H:\mg\mgComponents\" "NOT(~* X~ OR ~* LOCKED~)"
H:\mg>git-bash

jisqhx3@WIN7-7160 /h/mg (master)
$ git status
$ git add .
$ git commit -m"commit comments"
$ git push
```

The batch QCfile.bat can perform the backup as well (without the git part)  

* List the backup files with path: H:\mg\QCfile l
* Download backup files from QC to local drive: H:\mg\QCfile s
* Upload only MGOE_CreateLoad.xls: H:\mg\QCfile u


### After QC access password changed
* Run Encript.vbs:  

```
C:\tmp\cscript EncryptP.vbs "mypassword" TMS
```  
* Then copy displayed the encrypted password to replace the following lines in QCResource.vbs **and** ReadComponent.vbs:  

```
    strPassword = Decrypt("encrypted", "TMS")
```