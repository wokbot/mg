' ### started by copy ReadComponent.vbs 12:58 PM 10/31/2014
' ### feature is to copy relavent QC Resources files to local Drive
' ### track time spent on this
' ### record hours, complete time for this utility
' ### Completed 5:13 PM same Day
' ### substract approximately 10 minutes
' ### Version 2 changed to QCResource.vbs as upload is implemented
' ### Time spent for the change and test: 3 hours in the morning of 11/5/2014

Dim temp, key
Const intL = 32, intU = 126

Sub PasswordTestCommented
    temp = "plaintext"
    key = "TMS"

    temp = Encrypt(temp,key)
    WScript.Echo temp
    temp1 = "+cdXZdm"
    temp = Decrypt(temp,key)
    WScript.Echo temp
    WScript.Echo Decrypt(temp1,key)

    'wscript.echo ModShift(170, 32, 127)
End Sub

Function ModShift(intNumber)
    ModShift = ((intNumber - intL) mod (intU - intL)) + intL
    Do While ModShift < intL
        ModShift = ModShift + intU -intL
    Loop
End Function

Function Decrypt(str,key)
 Dim lenKey, KeyPos, LenStr, x, Newstr
 
 Newstr = ""
 lenKey = Len(key)
 KeyPos = 1
 LenStr = Len(Str)
 
 str=StrReverse(str)
 For x = LenStr To 1 Step -1
      '  WScript.Echo ModShift(asc(Mid(str,x,1)) - Asc(Mid(key,KeyPos,1)))
      Newstr = Newstr & chr(ModShift(asc(Mid(str,x,1)) - Asc(Mid(key,KeyPos,1))))
      KeyPos = KeyPos+1
      If KeyPos > lenKey Then KeyPos = 1
      Next
      Newstr=StrReverse(Newstr)
      Decrypt = Newstr
End Function

' ### This doesn't need QTP to copy Resource to local
' But the script has to run with C:\Windows\SysWOW64\wscript.exe or cscript.exe
' \Windows\SysWOW64\cscript QCResource.vbs <arguments> ... or
' \Windows\SysWOW64\wscript QCResource.vbs <arguments> ...
'

Function SyncCopyResource(strCommand, strLocalPath, strFilter)
	Set objFSO = CreateObject("Scripting.FileSystemObject")
    ' ### If using QTP
    'Set qtApp = CreateObject("QuickTest.Application")
    'qtApp.Launch
	'qtApp.Visible = True
	'If Not qtApp.TDConnection.IsConnected Then
		'MsgBox "Connect QTP to QC then try again..."
	'End If
    ' ### when not using QTP
	Set tdc = CreateObject("TDApiOle80.TDConnection")
	tdc.InitConnectionEx "http://hpqcenter:8080/qcbin"
    strPassword = Decrypt("+cdXZdm", "TMS")
	tdc.Login "jisqhx3",strPassword
	tdc.Connect "JBHUNT", "DailyTesting"
    ' ### get resource folder filter fields:
    REM for i = 1 to tdc.QCResourceFolderFactory.Filter.Fields.Count
        REM wscript.echo tdc.QCResourceFolderFactory.Filter.Fields.Item(i)
    REM next
    REM Wscript.quit
    
    REM RFO_DESCRIPTION
    REM RFO_ID
    REM RFO_NAME
    REM RFO_PARENT_ID
    REM RFO_PATH
    REM RFO_VER_STAMP
    REM RFO_USER_01    

    ' ### get resource filter fields:
    REM for i = 1 to tdc.QCResourceFactory.Filter.Fields.Count
        REM wscript.echo tdc.QCResourceFactory.Filter.Fields.Item(i)
    REM next
    REM Wscript.quit
    
    REM RSC_HAS_DEPENDENCIES
    REM RSC_DEV_COMMENTS
    REM RSC_CREATED_BY
    REM RSC_CREATION_DATE
    REM RSC_DESCRIPTION
    REM RSC_FILE_NAME
    REM RSC_PARENT_ID
    REM RSC_LOCATION_TYPE
    REM RSC_VTS
    REM RSC_NAME
    REM RSC_FOLDER_NAME
    REM RSC_ID
    REM RSC_TYPE
    REM RSC_VC_CHECKIN_COMMENTS
    REM RSC_VC_CHECKIN_DATE
    REM RSC_VC_CHECKIN_TIME
    REM RSC_VC_CHECKOUT_COMMENTS
    REM RSC_VC_CHECKOUT_DATE
    REM RSC_VC_CHECKOUT_TIME
    REM RSC_VC_CHECKIN_USER_NAME
    REM RSC_VC_CHECKOUT_USER_NAME
    REM RSC_VC_VERSION_NUMBER
    REM RSC_VER_STAMP
    REM RSC_VC_STATUS
    REM RSC_USER_01
    
    
    ' ### get compoent folder filter fields:
    REM for i = 1 to tdc.ComponentFolderFactory.FolderByPath("Components\MIB\MG").ComponentFactory.Filter.Fields.Count
        REM wscript.echo tdc.ComponentFolderFactory.FolderByPath("Components\MIB\MG").ComponentFactory.Filter.Fields.Item(i)
    REM next
    REM Wscript.quit
    
    'wscript.echo strFilter
	Set objFilter = tdc.QCResourceFactory.Filter
	'objFilter.Filter("RSC_NAME") = strFilter
	objFilter.Filter("RSC_FILE_NAME") = strFilter
    'wscript.echo strFilter
    'wscript.echo objFilter.Text
    'objFilter.Filter("CO_NAME") = "Not(""* X"")"
	Set objResourceList = tdc.QCResourceFactory.NewList(objFilter.Text)
'    WScript.Echo objFilter.Text
	intCount = objResourceList.Count
    'wscript.echo intCount
    strNameList =""
	For i = 1 to intCount
		strName = objResourceList.Item(i).FileName ' each or Name?
        Set objItem = objResourceList.Item(i)
        'wscript.echo strName
        strNameList = strNameList & vbCrlf & i & ": """ & strLocalPath & strName & """"
        'wscript.echo strNameList
        If strCommand = "s" Then
            WScript.StdOut.Write "Save " & i & ": """ & strLocalPath & strName & """"
            objItem.DownloadResource strLocalPath, True
            WScript.StdOut.WriteLine " done"
        ElseIf strCommand = "u" Then
            WScript.StdOut.Write "Upload " & i & ": """ & strLocalPath & strName & """"
            objItem.Post
            objItem.UploadResource strLocalPath, True
            WScript.StdOut.WriteLine " done"
        End If
	Next
    'wscript.echo strCommand
    If strCommand = "l" Then
        WScript.Echo "List:" & vbCrLf & strNameList
    End If
	SyncCopyScript = intCount
End Function

Sub TestCommented2
strCommand = "l"
strQCPath = "Resources\Test-MIB"
strLocalPath = "C:\tmp\mg\"
strFilter = """mg.xls"""
WScript.Echo strQCPath
WScript.Echo strLocalPath
WScript.Echo strFilter & vbCrlf
SyncCopyResource strCommand, strLocalPath, strFilter
End Sub

Set objArgs = WScript.Arguments
'WScript.Echo objArgs.Count
blnHelp = False
If objArgs.Count < 2 Then
    blnHelp = True
ElseIf Not (objArgs.Item(0) = "l" or objArgs.Item(0) = "s" or objArgs.Item(0) = "u") Then
    blnHelp = True
End If
If blnHelp Then
    WScript.Echo vbCrlf & "List, download or upload Resource files between QC and local drive" & vbCrlf & vbCrlf & _
    "Usage: " & "QCResource <command> <LocalPath> [<NameFilter>]"  & vbCrlf & vbCrlf & _
    "<command>" & vbCrlf & _
    "l: List Resource file names" & vbCrlf & _
    "s: Save Resource file from QC to local drive" & vbCrlf & _
    "u: upload Resource file from local drive to QC" & vbCrlf & vbCrlf & _
    "LocalPath: Local drive path. e.g. ""C:\tmp\mg\""" & vbCrlf & _
    "Name Filter for Name field only: Name of the Resource. e.g. ""test comp1"" or ""mg*"", default is *"  & vbCrlf & vbCrlf & _
    "Note: Filter supports wildcard, using ~ to replace double quote." & vbCrlf & _
    "e.g. names that not end with ""space X"": ""Not(~* X~)"" for ""Not(""* X""))"""
    WScript.Quit
End If

'TestCommented2

Sub MainSub
    strCommand = objArgs.Item(0)
    'strQCPath = objArgs.Item(1)
    strLocalPath = objArgs.Item(1)
    'WScript.Echo objArgs.Item(2)
    If objArgs.Count > 2 Then
        strFilter = replace(objArgs.Item(2), "~", chr(34))
    Else
        strFilter = """*"""
    End If
    If Right(strLocalPath, 1) <> "\" Then
        strLocalPath = strLocalPath & "\"
    End If


    'WScript.Echo "QC Path: " & strQCPath
    WScript.Echo "Local Path: " & strLocalPath
    WScript.Echo "Name Filter: " & strFilter & vbCrlf

    SyncCopyResource strCommand, strLocalPath, strFilter
End Sub

MainSub
