@echo off
setlocal
if "%1" == "l" goto :list
if "%1" == "s" goto :download
if "%1" == "u" goto :upload
goto help

echo call /?

:list
echo on
C:\Windows\SysWOW64\cscript QCResource.vbs l H:\mg\mgResources\ mg*
C:\Windows\SysWOW64\cscript ReadComponent.vbs l Components\MIB\MG H:\mg\mgComponents\ "Not(~* X~) and Not(~* LOCKED~)"
C:\Windows\SysWOW64\cscript ReadComponent.vbs l Components\MIB\MGOE H:\mg\mgComponents\ "Not(~* X~) and Not(~* LOCKED~)"
@echo off
goto end

:download
echo.
SET /P AREYOUSURE=You will read QC resources and Components to overwrite local files. (Select N and Use "QCfile l" to see the list.) Are you sure to proceed (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END
echo on
C:\Windows\SysWOW64\cscript QCResource.vbs s H:\mg\mgResources\ mg*
C:\Windows\SysWOW64\cscript ReadComponent.vbs s Components\MIB\MG H:\mg\mgComponents\ "Not(~* X~) and Not(~* LOCKED~)"
C:\Windows\SysWOW64\cscript ReadComponent.vbs s Components\MIB\MGOE H:\mg\mgComponents\ "Not(~* X~) and Not(~* LOCKED~)"
@echo off
goto end

:upload
C:\Windows\SysWOW64\cscript QCResource.vbs l H:\mg\mgResources\ MGOE_CreateLoad.xls
@echo off
echo.
SET /P AREYOUSURE=You will upload the file to QC listed above. The file in QC will be overwritten. Are you sure to proceed (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END
echo on
C:\Windows\SysWOW64\cscript QCResource.vbs u H:\mg\mgResources\ MGOE_CreateLoad.xls
@echo off
goto end

:help
echo.
echo QCfile [l^|s^|u] l:list; s:download and save to local; u:upload datatable to QC

:end
endlocal